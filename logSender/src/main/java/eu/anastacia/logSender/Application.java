package eu.anastacia.logSender;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;

@SpringBootApplication
public class Application {

    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public MessageListener messageListener() {
        return new MessageListener();
    }

    public static class MessageListener {

        private CountDownLatch latch = new CountDownLatch(3);

        @KafkaListener(topics = "${message.topic.name}", group = "log", containerFactory = "logKafkaListenerContainerFactory")
        public void listenLogs(String message , @Header(KafkaHeaders.RECEIVED_TIMESTAMP) long ts) {
            Logger logger = LogManager.getLogger();
            logger.info(message);
            ZonedDateTime now = ZonedDateTime.now( ZoneOffset.UTC );
            System.out.println("Sent datetime: " + now.toString());
            System.out.println(message);
            latch.countDown();
        }
    }
}
