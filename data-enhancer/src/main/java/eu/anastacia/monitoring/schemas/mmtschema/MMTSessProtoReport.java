package eu.anastacia.monitoring.schemas.mmtschema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class MMTSessProtoReport extends MMTReport{
    @JsonPropertyOrder({
        "protocolPath",
        "dataVolume",
        "payloadVolume",
        "packetCount",
        "upDataVol",
        "upPayVol",
        "upPkgCnt",
        "downDataVol",
        "downPayVol",
        "downPkgCnt",
        "clientIP",
        "serverIP",
        "sourceMAC",
        "destMAC",
        "clientPort",
        "serverPort"
    })
    /* TODO
     * Implement sub clases to extract the information from the extension reports,
     * which depend on the value of the column number 37.
     */

    // Positions of each field
    private static final int REPORT_NUMBER = 5 - MMTReport.DISPLACEMENT;
    // private static final int PROTOCOL_ID = 6 - MMTReport.DISPLACEMENT;
    // private static final int PROTOCOL_PATH_UPLINK = 7 - MMTReport.DISPLACEMENT;
    private static final int PROTOCOL_PATH_DOWNLINK = 8 - MMTReport.DISPLACEMENT;
    // private static final int NB_ACTIVE_FLOWS = 9 - MMTReport.DISPLACEMENT;
    private static final int DATA_VOLUME = 10 - MMTReport.DISPLACEMENT;
    private static final int PAYLOAD_VOLUME = 11 - MMTReport.DISPLACEMENT;
    private static final int PACKET_COUNT = 12 - MMTReport.DISPLACEMENT;
    private static final int UL_DATA_VOLUME = 13 - MMTReport.DISPLACEMENT;
    private static final int UL_PAYLOAD_VOLUME = 14 - MMTReport.DISPLACEMENT;
    private static final int UL_PACKET_COUNT = 15 - MMTReport.DISPLACEMENT;
    private static final int DL_DATA_VOLUME = 16 - MMTReport.DISPLACEMENT;
    private static final int DL_PAYLOAD_VOLUME = 17 - MMTReport.DISPLACEMENT;
    private static final int DL_PACKET_COUNT = 18 - MMTReport.DISPLACEMENT;
    // private static final int START_TIMESTAMP = 19 - MMTReport.DISPLACEMENT;
    private static final int CLIENT_ADDRESS = 20 - MMTReport.DISPLACEMENT;
    private static final int SERVER_ADDRESS = 21 - MMTReport.DISPLACEMENT;
    private static final int MAC_SOURCE = 22 - MMTReport.DISPLACEMENT;
    private static final int MAC_DESTINATION = 23 - MMTReport.DISPLACEMENT;
    // private static final int SESSION_ID = 24 - MMTReport.DISPLACEMENT;
    // private static final int SERVER_PORT = 25 - MMTReport.DISPLACEMENT;
    private static final int CLIENT_PORT = 26 - MMTReport.DISPLACEMENT;
    // private static final int THREAD_NUMBER = 27 - MMTReport.DISPLACEMENT;
    // private static final int RTT = 28 - MMTReport.DISPLACEMENT;
    // private static final int RTT_MIN_SERVER = 29 - MMTReport.DISPLACEMENT;
    // private static final int RTT_MIN_CLIENT = 30 - MMTReport.DISPLACEMENT;
    // private static final int RTT_MAX_SERVER = 31 - MMTReport.DISPLACEMENT;
    // private static final int RTT_MAX_CLIENT = 32 - MMTReport.DISPLACEMENT;
    // private static final int RTT_AVG_SERVER = 33 - MMTReport.DISPLACEMENT;
    // private static final int RTT_AVG_CLIENT = 34 - MMTReport.DISPLACEMENT;
    // private static final int DATA_TRANSFER_TIME = 35 - MMTReport.DISPLACEMENT;
    // private static final int RETRANSMISSION_COUNT = 36 - MMTReport.DISPLACEMENT;

    // Fields to be filled when processing
    protected String protocolPath;
    protected int dataVolume;
    protected int payloadVolume;
    protected int packetCount;
    protected int upDataVol;
    protected int upPayVol;
    protected int upPkgCnt;
    protected int downDataVol;
    protected int downPayVol;
    protected int downPkgCnt;
    protected String clientIP;
    protected String serverIP;
    protected String sourceMAC;
    protected String destMAC;
    protected int clientPort;
    protected int serverPort;

    protected MMTSessProtoReport(){
        this.reportType = "statistics-session";
    }

    @JsonIgnore
    @Override
    public boolean processReport(String[] data){
        this.protocolPath = data[MMTSessProtoReport.PROTOCOL_PATH_DOWNLINK];
        this.dataVolume = Integer.parseInt(data[MMTSessProtoReport.DATA_VOLUME]);
        this.payloadVolume = Integer.parseInt(data[MMTSessProtoReport.PAYLOAD_VOLUME]);
        this.packetCount = Integer.parseInt(data[MMTSessProtoReport.PACKET_COUNT]);
        this.upDataVol = Integer.parseInt(data[MMTSessProtoReport.UL_DATA_VOLUME]);
        this.upPayVol = Integer.parseInt(data[MMTSessProtoReport.UL_PAYLOAD_VOLUME]);
        this.upPkgCnt = Integer.parseInt(data[MMTSessProtoReport.UL_PACKET_COUNT]);
        this.downDataVol = Integer.parseInt(data[MMTSessProtoReport.DL_DATA_VOLUME]);
        this.downPayVol = Integer.parseInt(data[MMTSessProtoReport.DL_PAYLOAD_VOLUME]);
        this.downPkgCnt = Integer.parseInt(data[MMTSessProtoReport.DL_PACKET_COUNT]);
        this.clientIP = data[MMTSessProtoReport.CLIENT_ADDRESS];
        this.serverIP = data[MMTSessProtoReport.SERVER_ADDRESS];
        this.sourceMAC = data[MMTSessProtoReport.MAC_SOURCE];
        this.destMAC = data[MMTSessProtoReport.MAC_DESTINATION];
        this.clientPort = Integer.parseInt(data[MMTSessProtoReport.CLIENT_PORT]);
        this.serverPort = Integer.parseInt(data[MMTSessProtoReport.CLIENT_PORT]);

        return true;
    }

    public String getProtocolPath() {
        return this.protocolPath;
    }

    public int getDataVolume() {
        return this.dataVolume;
    }

    public int getPayloadVolume() {
        return this.payloadVolume;
    }

    public int getPacketCount() {
        return this.packetCount;
    }

    public int getUpDataVol() {
        return this.upDataVol;
    }

    public int getUpPayVol() {
        return this.upPayVol;
    }

    public int getUpPkgCnt() {
        return this.upPkgCnt;
    }

    public int getDownDataVol() {
        return this.downDataVol;
    }

    public int getDownPayVol() {
        return this.downPayVol;
    }

    public int getDownPkgCnt() {
        return this.downPkgCnt;
    }

    public String getClientIP() {
        return this.clientIP;
    }

    public String getServerIP() {
        return this.serverIP;
    }

    public String getSourceMAC() {
        return this.sourceMAC;
    }

    public String getDestMAC() {
        return this.destMAC;
    }

    public int getClientPort() {
        return this.clientPort;
    }

    /* Setters are not used
    public int getServerPort() {
        return this.serverPort;
    }
    
    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }
    
    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }
    
    public void setDestMAC(String destMAC) {
        this.destMAC = destMAC;
    }
    
    public void setSourceMAC(String sourceMAC) {
        this.sourceMAC = sourceMAC;
    }
    
    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }
    
    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }
    
    public void setDownPkgCnt(int downPkgCnt) {
        this.downPkgCnt = downPkgCnt;
    }
    
    public void setDownPayVol(int downPayVol) {
        this.downPayVol = downPayVol;
    }
    
    public void setDownDataVol(int downDataVol) {
        this.downDataVol = downDataVol;
    }
    
    public void setUpPkgCnt(int upPkgCnt) {
        this.upPkgCnt = upPkgCnt;
    }
    
    public void setUpPayVol(int upPayVol) {
        this.upPayVol = upPayVol;
    }
    
    public void setUpDataVol(int upDataVol) {
        this.upDataVol = upDataVol;
    }
    
    public void setPacketCount(int packetCount) {
        this.packetCount = packetCount;
    }
    
    public void setPayloadVolume(int payloadVolume) {
        this.payloadVolume = payloadVolume;
    }
    
    public void setDataVolume(int dataVolume) {
        this.dataVolume = dataVolume;
    }
    
    public void setProtocolPath(String protocolPath) {
        this.protocolPath = protocolPath;
    }
    */
}