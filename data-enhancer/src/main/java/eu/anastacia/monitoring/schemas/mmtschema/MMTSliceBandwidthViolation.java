package eu.anastacia.monitoring.schemas.mmtschema;

import com.fasterxml.jackson.databind.JsonNode;

public class MMTSliceBandwidthViolation extends MMTSecurityReport{

    protected MMTSliceBandwidthViolation(String historyJSON){
        // Just call the parent constructor to fill the identifier of the object
        // and parse the history JSON
        super(historyJSON);
    }

	@Override
	public boolean processHistory() {
        // D. Rivera - 06/08/2018
        // Bugfix - null is not supported by XL-SIEM. Send an empty string instead.
        // Search the attributes for the source and destination IP
        JsonNode attributes = this.rootHistoryJSON.get("event_1").get("attributes");
        if(attributes.isArray()){
            for(JsonNode attrib : attributes){
                if(attrib.get(0).asText().equals("ethernet.src")){
                    this.sourceMAC = attrib.get(1).asText();
                    continue;
                }
                if(attrib.get(0).asText().equals("ethernet.dst")){
                    this.destMAC = attrib.get(1).asText();
                    continue;
                }
                if(attrib.get(0).asText().equals("8021q.vid")){
                    this.vlanID = attrib.get(1).asInt();
                    continue;
                }
                if(attrib.get(0).asText().equals("udp.src_port")){
                    this.sourcePort = attrib.get(1).asInt();
                    continue;
                }
                if(attrib.get(0).asText().equals("udp.dest_port")){
                    this.destPort = attrib.get(1).asInt();
                    continue;
                }
                if(attrib.get(0).asText().equals("ip.proto_id")){
                    if(attrib.get(1).asInt() == 17)
                    this.protoName = "UDP";
                    continue;
                }
            }
        }
        this.sourceIP = "";
        this.destIP = "";
		return true;
	}
}