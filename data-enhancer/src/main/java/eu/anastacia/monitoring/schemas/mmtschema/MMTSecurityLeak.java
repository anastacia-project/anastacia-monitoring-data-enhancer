package eu.anastacia.monitoring.schemas.mmtschema;

import com.fasterxml.jackson.databind.JsonNode;

public class MMTSecurityLeak extends MMTSecurityReport{

    protected MMTSecurityLeak(String historyJSON){
        // Just call the parent constructor to fill the identifier of the object
        // and parse the history JSON
        super(historyJSON);
    }

	@Override
	public boolean processHistory() {
        // D. Rivera - 06/08/2018
        // Bugfix - null is not supported by XL-SIEM. Send an empty string instead.
        // Search the attributes for the source and destination IP
        JsonNode attributes = this.rootHistoryJSON.get("event_1").get("attributes");
        String ip1 = null, ip2 = null;
        if(attributes.isArray()){
            for(JsonNode attrib : attributes){
                if(attrib.get(0).asText().equals("ip.src") || attrib.get(0).asText().equals("ipv6.src")){
                    ip1 = attrib.get(1).asText();
                    continue;
                }
                if(attrib.get(0).asText().equals("ip.dst") || attrib.get(0).asText().equals("ipv6.dst")){
                    ip2 = attrib.get(1).asText();
                    continue;
                }
            }
        }
        this.sourceIP = ip1;
        this.destIP = ip2;
        this.sourceMAC = "";
        this.destMAC = "";
		return true;
	}
}