package eu.anastacia.monitoring.schemas.mmtschema;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class MMTSecurityReport extends MMTReport{
    @JsonPropertyOrder({
        "propertyID",
        "verdict",
        "securityType",
        "cause",
        "sourceIP",
        "destIP",
        "sourceMAC",
        "destMAC",
        "vlanID"
    })
    // Positions of each field
    private static final int PROPERTY_ID = 5 - MMTReport.DISPLACEMENT;
    private static final int VERDICT = 6 - MMTReport.DISPLACEMENT;
    private static final int TYPE = 7 - MMTReport.DISPLACEMENT;
    private static final int CAUSE = 8 - MMTReport.DISPLACEMENT;
    private static final int HISTORY = 9 - MMTReport.DISPLACEMENT;

    // Fields to be filled when processing
    protected int propertyID;
    protected String verdict;
    protected String securityType;
    protected String cause;
    protected String sourceIP;
    protected String destIP;
    protected String sourceMAC;
    protected String destMAC;
    protected int vlanID;
    @JsonIgnore
    protected int sourcePort;
    @JsonIgnore
    protected int destPort;
    @JsonIgnore
    protected String protoName;
    

    // Internal members to parse the history object
    @JsonIgnore
    protected transient JsonNode rootHistoryJSON;

    protected MMTSecurityReport(String historyJSON){
        // If this constructor was called, we know we are dealing with a security report
        // Let's take advantage of this instance to parse the "history" JSON object
        ObjectMapper mapper = new ObjectMapper();
        try{
            this.rootHistoryJSON = mapper.readTree(historyJSON);
            this.reportType = "security";
        } catch (IOException e){
            System.out.println("Error parsing history member of the security Report!");
            e.printStackTrace();
        }
    }

    @JsonIgnore
    public static MMTSecurityReport getMMTSecurityReport(String[] data){
        // The dynamic type of the returned object depends on the Property ID.
        // Extend this part and implement the corresponding class to support more properties
        // NOTE! The history JSON HAS TO BE passed to the constructor
        switch(data[MMTSecurityReport.PROPERTY_ID]){
            case "60":
                return new MMTIoTICMPAttack(data[MMTSecurityReport.HISTORY]);
            case "67":
                return new MMTIoTSQLiAttack(data[MMTSecurityReport.HISTORY]);
            case "77":
                return new MMTSlowCommAttack(data[MMTSecurityReport.HISTORY]);
            case "97":
                return new MMTOutsiderConnection(data[MMTSecurityReport.HISTORY]);
            case "98":
                return new MMTSliceBandwidthViolation(data[MMTSecurityReport.HISTORY]);
            case "99":
                return new MMTSecurityLeak(data[MMTSecurityReport.HISTORY]);
            default:
                throw new UnsupportedOperationException("MMT Security Property with ID " + data[MMTSecurityReport.PROPERTY_ID] + " is not supported!");
        }
    }

    @JsonIgnore
    @Override
    public boolean processReport(String[] data){
        this.propertyID = Integer.parseInt(data[MMTSecurityReport.PROPERTY_ID]);
        this.verdict = data[MMTSecurityReport.VERDICT];
        this.securityType = data[MMTSecurityReport.TYPE];
        this.cause = data[MMTSecurityReport.CAUSE];

        // Since the structure of the "history" object depends on the tested security property
        // sublcases will implement the proper way of processing this informaiton correctly
        return this.processHistory();
    }

    /*
     * This is the method each subclass needs to implement
     * in order to correctly parse thehistory JSON object
     */
    @JsonIgnore
    public abstract boolean processHistory();



    public int getPropertyID() {
        return this.propertyID;
    }

    public String getVerdict() {
        return this.verdict;
    }

    public String getSecurityType() {
        return this.securityType;
    }

    public String getCause() {
        return this.cause;
    }

    public String getSourceIP() {
        return this.sourceIP;
    }

    public String getDestIP() {
        return this.destIP;
    }

    public String getSourceMAC() {
        return this.sourceMAC;
    }

    public String getDestMAC() {
        return this.destMAC;
    }

    public int getVlanID() {
        return vlanID;
    }
    
    /* Setters are not used
    public void setDestMAC(String destMAC) {
        this.destMAC = destMAC;
    }
    
    public void setSourceMAC(String sourceMAC) {
        this.sourceMAC = sourceMAC;
    }
    
    public void setDestIP(String destIP) {
        this.destIP = destIP;
    }
    
    public void setSourceIP(String sourceIP) {
        this.sourceIP = sourceIP;
    }
    
    public void setCause(String cause) {
        this.cause = cause;
    }
    
    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }
    
    public void setVerdict(String verdict) {
        this.verdict = verdict;
    }
    
    public void setPropertyID(int propertyID) {
        this.propertyID = propertyID;
    }
    */
}