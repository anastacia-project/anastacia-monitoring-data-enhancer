package eu.anastacia.monitoring.schemas.mmtschema;

import com.fasterxml.jackson.databind.JsonNode;

public class MMTIoTICMPAttack extends MMTSecurityReport{

    protected MMTIoTICMPAttack(String historyJSON){
        // Just call the parent constructor to fill the identifier of the object
        // and parse the history JSON
        super(historyJSON);
    }

	@Override
	public boolean processHistory() {
        // D. Rivera - 06/08/2018
        // Bugfix - null is not supported by XL-SIEM. Send an empty string instead.
        this.sourceIP = "";
        this.sourceMAC = "";
        // At this point, the parse of the root object is done
        this.destIP = "";
        this.destMAC = "";
        // Extract the MAC of each event
        String ip1, ip2, ip3;
        // Obtain the attribtes array from the first event
        JsonNode attributes1 = this.rootHistoryJSON.get("event_1").get("attributes");
        // From the first member, extract the "lowpan.iphc_dst" value as string
        ip1 = attributes1.get(1).get("lowpan.iphc_dst").asText();
        // Obtain the attribtes array from the second event
        JsonNode attributes2 = this.rootHistoryJSON.get("event_2").get("attributes");
        // From the first member, extract the "lowpan.iphc_dst" value as string
        ip2 = attributes2.get(1).get("lowpan.iphc_dst").asText();
        // Obtain the attribtes array from the third event
        JsonNode attributes3 = this.rootHistoryJSON.get("event_3").get("attributes");
        // From the first member, extract the "lowpan.iphc_dst" value as string
        ip3 = attributes3.get(1).get("lowpan.iphc_dst").asText();

        // Compare the extracted macs, they DO HAVE TO MATCH
        if(ip1.equals(ip2) && ip2.equals(ip3))
            this.destIP = ip1;
        else
            this.destIP = "";
        // TODO - Obtain the MAC Address
        this.destMAC = "";
		return false;
	}
}