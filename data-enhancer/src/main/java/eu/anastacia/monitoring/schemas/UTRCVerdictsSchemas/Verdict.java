package eu.anastacia.monitoring.schemas.UTRCVerdictsSchemas;

/**
 * @author Konstantinos Theodosiou
 */
public class Verdict {

    private String origin;
    private String timestamp;
    private Boolean attack;
    private String severity;
    private String score;
    private String explanation;


    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getAttack() {
        return attack;
    }

    public void setAttack(Boolean attack) {
        this.attack = attack;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }
}
