package eu.anastacia.monitoring.schemas.IoTSchemas;

public class ContextResponses {
    ContextElement contextElement;
    StatusCode statusCode;

    public ContextElement getContextElement() {
        return contextElement;
    }

    public void setContextElement(ContextElement contextElement) {
        this.contextElement = contextElement;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }
}


