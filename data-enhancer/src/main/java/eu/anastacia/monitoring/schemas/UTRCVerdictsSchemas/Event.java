package eu.anastacia.monitoring.schemas.UTRCVerdictsSchemas;

/**
 * @author Konstantinos Theodosiou
 */
public class Event {

    private String ts;
    private String val;


    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
