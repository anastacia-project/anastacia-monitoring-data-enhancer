package eu.anastacia.monitoring.schemas.mmtschema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "reportType"
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = eu.anastacia.monitoring.schemas.mmtschema.MMTSecurityReport.class, name = "security"),
    @JsonSubTypes.Type(value = eu.anastacia.monitoring.schemas.mmtschema.MMTSessProtoReport.class, name = "statistics-session"),
    @JsonSubTypes.Type(value = eu.anastacia.monitoring.schemas.mmtschema.MMTSessNoProtoReport.class, name = "statistics-no-session")
})
public abstract class MMTReport{

    // This is the displacement used. It was introduced to be in line with
    // the documenentation of MMT Probe
    protected static final int DISPLACEMENT = 1;

    // This is the list of common fields of each report
    // according to the documentaiton in BitBucket
    private static final int FORMAT_ID = 1 - MMTReport.DISPLACEMENT;
    private static final int PROBE = 2 - MMTReport.DISPLACEMENT;
    private static final int SOURCE = 3 - MMTReport.DISPLACEMENT;
    private static final int TIMESTAMP = 4 - MMTReport.DISPLACEMENT;

    // Data that will be transfered to the reformated message
    protected String reportType; // To be filled accordingly in the subclasses
    protected int probeID;
    protected String source;
    protected double timestamp;

    public static MMTReport processData(String csv){
        MMTReport report = null;
        String[] data = MMTReport.splitter(csv);
        //Process the information of the type of report
        switch(data[MMTReport.FORMAT_ID]){
            case "10":
                // Security Report
                // The dynamic type will be determined according to the data
                // by the getMMTSecurityReport method
                report = MMTSecurityReport.getMMTSecurityReport(data);
                report.processReport(data);
                break;
            case "99":
                // Session with no protocol Report
                report = new MMTSessNoProtoReport();
                report.processReport(data);
                break;
            case "100":
                // Session with protocol Report
                report = new MMTSessProtoReport();
                report.processReport(data);
                break;
            default:
                throw new UnsupportedOperationException("The format "+data[MMTReport.FORMAT_ID]+" is not supppoted.");
        }
        // Process the basic information
        report.probeID = Integer.parseInt(data[MMTReport.PROBE]);
        report.source = data[MMTReport.SOURCE];
        report.timestamp = Double.parseDouble(data[MMTReport.TIMESTAMP]);
        return report;
    }

    private static String[] splitter(String csvData){
        String[] preliminar = csvData.split(",");
        String[] result;
        // We have to be careful with the security reports
        // The last line COULD HAVE SPLITTED the JSON object
        // So, if a security report is detected, a special treatment
        // needs to be done
        switch(preliminar[MMTReport.FORMAT_ID]){
            case "10":
                // This is a security report. It has exactly 9 columns
                preliminar = csvData.split(",",9);
                // Sanitization of the first 8 columns
                // This could be elegantly done using a lambda
                result = new String[preliminar.length];
                for(int i=0; i<8; i++){
                    result[i] = preliminar[i].replace("\"", "");
                }
                // Of course, the last column has to pass unchanged
                result[8] = preliminar[8];
                return result;
            default:
                // Any other report, split the data in as many columns as possible
                // Sanitization of the columns
                result = new String[preliminar.length];
                for(int i=0; i<preliminar.length; i++){
                    result[i] = preliminar[i].replace("\"", "");
                }
                return result;
        }
    }

    @JsonIgnore
    public String getJSON(){
        // Create the JSON Builder and return the JSON representation of the object.
        ObjectMapper mapper = new ObjectMapper();
        try{
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e){
            System.out.println("Could not get JSON representation of the message!");
            e.printStackTrace();
            return null;
        }
    }

    public String getReportType() {
        return this.reportType;
    }

    public int getProbeID() {
        return this.probeID;
    }

    public String getSource() {
        return this.source;
    }

    public double getTimestamp() {
        return this.timestamp;
    }

    /* Setter are not used */
    
    /*
    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }
    
    public void setSource(String source) {
        this.source = source;
    }
    
    public void setProbeID(int probeID) {
        this.probeID = probeID;
    }
    
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
    */

    // This method implements the specific treatment of each report
    public abstract boolean processReport(String[] data);
}