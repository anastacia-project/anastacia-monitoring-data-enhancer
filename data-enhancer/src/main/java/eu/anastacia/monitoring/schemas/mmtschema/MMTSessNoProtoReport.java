package eu.anastacia.monitoring.schemas.mmtschema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class MMTSessNoProtoReport extends MMTReport{
    @JsonPropertyOrder({
        "protocolPath",
        "dataVolume",
        "payloadVolume",
        "packetCount"
    })
    // Positions of each field
    private static final int REPORT_NUMBER = 5 - MMTReport.DISPLACEMENT;
    // private static final int PROTOCOL_ID = 6 - MMTReport.DISPLACEMENT;
    private static final int PROTOCOL_PATH = 7 - MMTReport.DISPLACEMENT;
    // private static final int NB_ACTIVE_FLOWS = 8 - MMTReport.DISPLACEMENT;
    private static final int DATA_VOLUME = 9 - MMTReport.DISPLACEMENT;
    private static final int PAYLOAD_VOLUME = 10 - MMTReport.DISPLACEMENT;
    private static final int PACKET_COUNT = 11 - MMTReport.DISPLACEMENT;
    // private static final int UL_DATA_VOLUME = 12 - MMTReport.DISPLACEMENT;
    // private static final int UL_PAYLOAD_VOLUME = 13 - MMTReport.DISPLACEMENT;
    // private static final int UL_PACKET_COUNT = 14 - MMTReport.DISPLACEMENT;
    // private static final int DL_DATA_VOLUME = 15 - MMTReport.DISPLACEMENT;
    // private static final int DL_PAYLOAD_VOLUME = 16 - MMTReport.DISPLACEMENT;
    // private static final int DL_PACKET_COUNT = 17 - MMTReport.DISPLACEMENT;
    // private static final int START_TIMESTAMP = 18 - MMTReport.DISPLACEMENT;
    // private static final int CLIENT_ADDRESS = 19 - MMTReport.DISPLACEMENT;
    // private static final int SERVER_ADDRESS = 20 - MMTReport.DISPLACEMENT;
    // private static final int MAC_SOURCE = 21 - MMTReport.DISPLACEMENT;
    // private static final int MAC_DESTINATION = 22 - MMTReport.DISPLACEMENT;
    // private static final int SESSION_ID = 23 - MMTReport.DISPLACEMENT;
    // private static final int SERVER_PORT = 24 - MMTReport.DISPLACEMENT;
    // private static final int CLIENT_PORT = 25 - MMTReport.DISPLACEMENT;

    // Fields to be filled when processing
    protected String protocolPath;
    protected int dataVolume;
    protected int payloadVolume;
    protected int packetCount;

    protected MMTSessNoProtoReport(){
        this.reportType = "statistics-no-session";
    }

    @JsonIgnore
    @Override
    public boolean processReport(String[] data){
        this.protocolPath = data[MMTSessNoProtoReport.PROTOCOL_PATH];
        this.dataVolume = Integer.parseInt(data[MMTSessNoProtoReport.DATA_VOLUME]);
        this.payloadVolume = Integer.parseInt(data[MMTSessNoProtoReport.PAYLOAD_VOLUME]);
        this.packetCount = Integer.parseInt(data[MMTSessNoProtoReport.PACKET_COUNT]);
        return true;
    }

    public String getProtocolPath() {
        return this.protocolPath;
    }

    public int getDataVolume() {
        return this.dataVolume;
    }

    public int getPayloadVolume() {
        return this.payloadVolume;
    }

    public int getPacketCount() {
        return this.packetCount;
    }

    /* Setters are not used
    
    public void setPacketCount(int packetCount) {
        this.packetCount = packetCount;
    }
    
    public void setPayloadVolume(int payloadVolume) {
        this.payloadVolume = payloadVolume;
    }
    
    public void setDataVolume(int dataVolume) {
        this.dataVolume = dataVolume;
    }
    
    public void setProtocolPath(String protocolPath) {
        this.protocolPath = protocolPath;
    }
    */
}