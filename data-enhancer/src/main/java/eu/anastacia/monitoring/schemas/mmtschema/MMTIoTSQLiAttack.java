package eu.anastacia.monitoring.schemas.mmtschema;

import com.fasterxml.jackson.databind.JsonNode;

public class MMTIoTSQLiAttack extends MMTSecurityReport{

    protected MMTIoTSQLiAttack(String historyJSON){
        // Just call the parent constructor to fill the identifier of the object
        // and parse the history JSON
        super(historyJSON);
    }

	@Override
	public boolean processHistory() {
        // At this point, the parse of the root object is done
        this.sourceIP = "";
        this.sourceMAC = "";
        // Obtain the attribtes array from the first event
        JsonNode attributes = this.rootHistoryJSON.get("event_1").get("attributes");
        // From the first member, extract the "ipv6.dst" value as string
        this.destIP = attributes.get(0).get("ipv6.dst").asText();
        // TODO - Obtain the destination MAC address
        this.destMAC = "";
		return false;
	}
}