package eu.anastacia.monitoring.schemas.UTRCVerdictsSchemas;

import java.util.List;

/**
 * @author Konstantinos Theodosiou
 */
public class Entity {

    Verdict verdict;
    private List<Event> events;


    public Verdict getVerdict() {
        return verdict;
    }

    public void setVerdict(Verdict verdict) {
        this.verdict = verdict;
    }


    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
