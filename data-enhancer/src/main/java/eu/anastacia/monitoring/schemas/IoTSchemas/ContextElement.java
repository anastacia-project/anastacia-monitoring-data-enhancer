package eu.anastacia.monitoring.schemas.IoTSchemas;

import java.util.List;

public class ContextElement {
    String type;
    String isPattern;
    String id;
    List<Attributes> attributes;


    public List<Attributes> getAttributes() {
        return attributes;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAttributes(List<Attributes> attributes) {
        this.attributes = attributes;
    }

    public String getIsPattern() {
        return isPattern;
    }

    public void setIsPattern(String isPattern) {
        this.isPattern = isPattern;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}