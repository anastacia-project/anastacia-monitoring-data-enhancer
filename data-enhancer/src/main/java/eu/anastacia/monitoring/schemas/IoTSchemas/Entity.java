package eu.anastacia.monitoring.schemas.IoTSchemas;

import java.util.List;

public class Entity {
    String subscriptionId;
    String originator;
    List<ContextResponses> contextResponses;


    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public List<ContextResponses> getContextResponses() {
        return contextResponses;
    }

    public void setContextResponses(List<ContextResponses> contextResponses) {
        this.contextResponses = contextResponses;
    }
}
