/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.anastacia.monitoring;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.kafka.*;
import org.apache.storm.mongodb.bolt.MongoInsertBolt;
import org.apache.storm.mongodb.common.mapper.MongoMapper;
import org.apache.storm.mongodb.common.mapper.SimpleMongoMapper;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This topology demonstrates Storm's stream groupings and multilang capabilities.
 */
public class DataEnhancer {

  public static class WordCount extends BaseBasicBolt {
    Map<String, Integer> counts = new HashMap<String, Integer>();

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
      DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); /*Enter the proper format according to instructions*/
      Date date = new Date();
      //System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
      String word = tuple.getString(0);
      String word1 =  dateFormat.format(date)+ word + "Anastacia";
      Integer count = counts.get(word1);
      if (count == null)
        count = 0;
      count++;
      counts.put(word1, count);
      collector.emit(new Values(word1, count));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("word", "count"));
    }
  }

  private static final String url = "mongodb://127.0.0.1:27017/test";   /*Mongo Settings*/
  private static final String collectionName = "anastacia_monitoring_data";
  private static final String INSERT_BOLT = "INSERT_BOLT";



  public static void main(String[] args) throws Exception {

    TopologyBuilder builder = new TopologyBuilder();

    MongoMapper mapper = new SimpleMongoMapper().withFields("word", "count");

    MongoInsertBolt insertBolt = new MongoInsertBolt(url, collectionName, mapper);
    String topicName = "cpu.report";

    BrokerHosts hosts = new ZkHosts("127.0.0.1:2181");
    SpoutConfig spoutConfig = new SpoutConfig(hosts, "cpu.report", "", topicName);
    spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
    KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);


    builder.setSpout("spout", kafkaSpout, 5);

    //builder.setSpout("spout", new RandomSentenceSpout(), 5);  /*Custom Spouts and Bolts*/
    //builder.setBolt("split", new SplitSentence(), 8).shuffleGrouping("spout");
    //builder.setBolt("count", new WordCount(), 12).shuffleGrouping("split");
    builder.setBolt("count", new WordCount(), 12).shuffleGrouping("spout");
      builder.setBolt(INSERT_BOLT, insertBolt, 1).fieldsGrouping("count", new Fields("word"));
    //builder.setBolt("count", new WordFind(),14)
    Config conf = new Config();
    conf.setDebug(true);


    if (args != null && args.length > 0) {
      conf.setNumWorkers(3);

      StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
    }
    else {
      conf.setMaxTaskParallelism(3);

      LocalCluster cluster = new LocalCluster();
      cluster.submitTopology("word-count", conf, builder.createTopology());

      Thread.sleep(10000);

      cluster.shutdown();
    }
  }
}
