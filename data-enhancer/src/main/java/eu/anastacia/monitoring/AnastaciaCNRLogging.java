package eu.anastacia.monitoring;

import java.io.IOException;
import java.util.Properties;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

public class AnastaciaCNRLogging extends BaseBasicBolt{

    private static final long serialVersionUID = -5063466336134581788L;
    private static final String BOOTSTRAP_SERVER = "anastacia-log.ge.ieiit.cnr.it:9092";
    //private static final String BOOTSTRAP_SERVER = "10.79.7.176:9092";
    //private static final String BOOTSTRAP_SERVER = "localhost:9092";
    private static final String TOPIC_NAME = "anastacia-log";

    private Properties props;
    private static Producer<String, String> producer;

    // Variables used to transfer the message
    private String fromComp;
    private String fromMod;
    private String toComp;
    private String toMod;

    public AnastaciaCNRLogging(String fromComp, String fromMod, String toComp, String toMod){
        // From AnastaciaLogger Class

        // create instance for properties to access producer configs
        props = new Properties();

        //Assign localhost id
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);

        //Set acknowledgements for producer requests.
        props.put(ProducerConfig.ACKS_CONFIG, "all");

        //If the request fails, the producer can automatically retry,
        props.put(ProducerConfig.RETRIES_CONFIG, 0);

        //Specify buffer size in config
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);

        //Reduce the no of requests less than 0
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);

        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);

        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<String, String>(props);

        this.fromComp = fromComp;
        this.fromMod = fromMod;
        this.toComp = toComp;
        this.toMod = toMod;
    }

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        String data = input.getString(0);
        ObjectMapper mapper = new ObjectMapper();
        // The received data SHOULD be a valid Json... Check this
        ObjectNode payload;
        try{
            payload = (ObjectNode) mapper.readTree(data);
        } catch (IOException e){
            System.out.println("The given data is not a valid Json");
            e.printStackTrace();
            return;
        }

        // Create the root node
        ObjectNode root = mapper.createObjectNode();
        // Take the actual timestamp
        long tstamp = System.currentTimeMillis();

        // Add a new value to the payload: a precise timestamp
        payload.put("detection-tstamp", tstamp);

        // Create the node structure
        root.put("timestamp", ((double) (tstamp)) / 1000.0);
        root.put("from_component", this.fromComp);
        root.put("from_module", this.fromMod);
        root.put("to_component", this.toComp);
        root.put("to_module", this.toMod);
        root.put("incoming", true);
        root.put("method", "Kafka-broker");
        root.set("data", payload);
        root.put("notes", "");

        try{
            producer.send(new ProducerRecord<String, String>(AnastaciaCNRLogging.TOPIC_NAME, System.currentTimeMillis() + "", mapper.writeValueAsString(root)));
        } catch (JsonProcessingException e){
            System.out.println("Exception while serializing the logger message");
            e.printStackTrace();
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        //???
    }

}