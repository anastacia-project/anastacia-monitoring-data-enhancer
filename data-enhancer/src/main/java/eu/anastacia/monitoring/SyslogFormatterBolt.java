package eu.anastacia.monitoring;

import com.cloudbees.syslog.Facility;
import com.cloudbees.syslog.Severity;
import com.cloudbees.syslog.SyslogMessage;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

public class SyslogFormatterBolt extends BaseBasicBolt{

    private static final long serialVersionUID = -1630667596359545041L;
    
    // AnastaciaLogger class to send to the Kafka Broker
    private static final AnastaciaLogger logger = new AnastaciaLogger();

    private String appName;
    private Facility fac;
    private Severity sev;
    private String hostname;

	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
        // Get the data from the input
        String rawData= input.getString(0);

        // Buffer to store the syslog formatted message
        String syslogFormatted = "";

        // Add the metadada to the syslog message
        SyslogMessage message = new SyslogMessage();
        message.withAppName(this.appName);
        message.withFacility(this.fac);
        message.withSeverity(this.sev);
        message.withTimestamp(System.currentTimeMillis());
        // Add the content
        message.withMsg(rawData);

        // Obtain a String representation of the syslog message
        syslogFormatted = message.toRfc5424SyslogMessage();

        // Send the message to the Kafka Broker
        logger.send((long) 0 , syslogFormatted);
        // Emit and ack the message
        //collector.emit(new Values(syslogFormatted));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("syslogMessage"));
    }

    /* Getters and Setters */
    
    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Facility getFacility() {
        return this.fac;
    }

    public void setFacility(Facility fac) {
        this.fac = fac;
    }

    public Severity getSeverity() {
        return this.sev;
    }

    public void setSeverity(Severity sev) {
        this.sev = sev;
    }

    public String getHostname() {
        return this.hostname;
    }
    
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}