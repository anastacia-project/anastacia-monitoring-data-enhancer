package eu.anastacia.monitoring;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import java.util.Properties;

public class AnastaciaLogger {

//    private final static String TOPIC = "MonitoringDataEnhancerLogs";
//    private final static String BOOTSTRAP_SERVERS = "212.101.173.57:9092";

    private final static String TOPIC = System.getenv("LOG_TOPIC");
    private final static String BOOTSTRAP_SERVERS = System.getenv("KAFKA_URL_PRODUCER");


    private Properties props;
    Producer<String, String> producer;

    public AnastaciaLogger(){
        // create instance for properties to access producer configs
        props = new Properties();

        //Assign localhost id
        props.put("bootstrap.servers", BOOTSTRAP_SERVERS);

        //Set acknowledgements for producer requests.
        props.put("acks", "all");

        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);

        //Specify buffer size in config
        props.put("batch.size", 16384);

        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);

        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);

//        props.put("key.serializer", "org.apache.kafka.common.serializa-tion.StringSerializer");
//        props.put("value.serializer", "org.apache.kafka.common.serializa-tion.StringSerializer");

        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<String, String>(props);
    }

    public void send(Long key, String data) {
        producer.send(new ProducerRecord<String, String>(TOPIC, Long.toString(key), data));
    }
}
