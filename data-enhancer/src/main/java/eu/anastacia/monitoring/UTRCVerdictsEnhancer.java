package eu.anastacia.monitoring;


import eu.anastacia.monitoring.schemas.UTRCVerdictsSchemas.Entity;
import eu.anastacia.monitoring.schemas.UTRCVerdictsSchemas.Event;
import eu.anastacia.monitoring.schemas.UTRCVerdictsSchemas.Verdict;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.kafka.*;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.codehaus.jackson.map.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Konstantinos Theodosiou
 */
public class UTRCVerdictsEnhancer {

    public static class DataEnhancer extends BaseBasicBolt {

        private static final AnastaciaLogger logger = new AnastaciaLogger();

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {

    // Jul 10 11:19:29 10.0.2.2 [UTRC] {'verdict': {'origin': 'IoT', 'timestamp': 1531218480229, 'attack': True, 'severity': 'low', 'score': '0.90', 'explanation': 'Probing attack'},'events': [{'ts': '2018.02.23 17:02:38', 'val': '23.5'}, {'ts': '2018.02.23 17:04:16', 'val': '13.0'}, {'ts': '2018.02.23 17:12:38', 'val': '23.0'}]}


            List<Object> values = tuple.getValues();
            for(Object obj : values){
                Entity entity = null;
                String data = "";
                String timestamp = "";



                ObjectMapper mapper = new ObjectMapper();
                try{
                    entity = mapper.readValue(obj.toString(), Entity.class);
                    System.out.println("\n\n\n" + obj.toString());

                    Verdict verdict = entity.getVerdict();
                    /*
                    "verdict": {
                    "origin": "IoT",
                    "timestamp": "07 10 13:05:00",
                    "attack": true,
                    "serverity": "medium",
                    "score": "0.80",
                    "explanation": "Active IoT scan"
                    }
                    */
                    //verdict attributes and their default values
                    String origin = "null";
                    //String verdictTimestamp = "null";
                    Boolean attack = false; // just used false as default value
                    String severity = "null";
                    String score = "null";
                    String explanation = "null";

                    origin = verdict.getOrigin();
                    timestamp = verdict.getTimestamp();
                    attack = verdict.getAttack();
                    severity = verdict.getSeverity();
                    score = verdict.getScore();
                    explanation = verdict.getExplanation();

                    // Jul 10 11:19:29 10.0.2.2 [UTRC] {'verdict': {'origin': 'IoT', 'timestamp': 1531218480229, 'attack': True, 'severity': 'low', 'score': '0.90', 'explanation': 'Probing attack'},'events': [{'ts': '2018.02.23 17:02:38', 'val': '23.5'}, {'ts': '2018.02.23 17:04:16', 'val': '13.0'}, {'ts': '2018.02.23 17:12:38', 'val': '23.0'}]}

                    // timestamp and attack don't use ''
                    data= " [UTRC] {\'verdict\':"
                            +" {\'origin\':\'"+ origin
                            +"\',\'timestamp\': " + "\'" + timestamp + "\'";

                            if(attack==false){
                                data = data +" ,\'attack\': " + "false";
                            }else{
                                data = data +" ,\'attack\': " + "true";
                            }

                            data = data +" ,\'severity\':\'" + severity
                            +"\',\'score\':\'" + score
                            +"\',\'explanation\':\'" + explanation
                            +"\'}" +",";

                    String eventsString =" \'events\': [" ;


                    List<Event> eventsList = entity.getEvents();
                    /*
                          "events": [
                    {
                      "ts": "2018.02.23 17:02:38",
                      "val": "23.5"
                    },....
                    */
                    if (eventsList == null || eventsList.isEmpty()) {
                        System.out.println("No events found");
                        eventsString += "]"; //send empty table if no events are received,

                    } else {

                        for (Event event : eventsList) {
                            String singleEventString = "";
                            //events attributes and their default values
                            String ts = "null";
                            String val = "null";

                            ts = event.getTs();
                            val = event.getVal();

                            ///'events': [{'ts': '2018.02.23 17:02:38', 'val': '23.5'},
                            singleEventString +=  " {\'ts\':\'"+ ts
                                    +"\',\'val\': " + "\'" + val + "\'"
                                    +"}" +","; //adding comma is needed in all events except the last one, so we remove it afterwards

                            eventsString +=singleEventString;
                        }

                        eventsString = eventsString .substring(0, eventsString.length()-1); //remove the last comma
                        eventsString += "]";

                    }


                    data +=eventsString + "}";


                    final String OLD_FORMAT = "MM dd HH:mm:ss";  //    2018-06-13/09:34:32
                    final String NEW_FORMAT = "MMM dd HH:mm:ss";

                    SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
                    Date d = sdf.parse(timestamp);
                    sdf.applyPattern(NEW_FORMAT);
                    String timestampFront = sdf.format(d);

                    //Tested with collecting and sending our own time, but will not be used
                    //Calendar cal = Calendar.getInstance();
                    //timestamp = dateFormat.format(cal.getTime());


                    data = timestampFront + data;

                    System.out.println(data + "\n\n\n");
                    //Produce data to the MonitoringDataEnhancerLogs topic
                    logger.send((long) 0, data);


                }
                catch (Exception e) {
                    System.out.println("Cast failed: " + e);
                }
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word", "count"));
        }

    }

    public static void main(String[] args) throws Exception {

        TopologyBuilder builder = new TopologyBuilder();


        String topicName = "UTRCVerdicts";
        String zkRoot = "";
        String consumerGroupId = "group1";

        String kafka_url = System.getenv("KAFKA_URL_CONSUMER");

        BrokerHosts hosts = new ZkHosts(kafka_url);

        SpoutConfig spoutConfig = new SpoutConfig(hosts, topicName, zkRoot, consumerGroupId);
        spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        builder.setSpout("spout", kafkaSpout, 5);
        builder.setBolt("count", new UTRCVerdictsEnhancer.DataEnhancer(), 12).shuffleGrouping("spout");

        Config conf = new Config();
        conf.setDebug(false);

        if (args != null && args.length > 0) {
            conf.setNumWorkers(3);
            StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
        }
        else {
            conf.setMaxTaskParallelism(3);
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("word-count", conf, builder.createTopology());

            cluster.wait();

            Thread.sleep(10000);
            cluster.shutdown();
        }
    }
}