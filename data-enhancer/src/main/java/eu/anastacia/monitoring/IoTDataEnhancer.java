package eu.anastacia.monitoring;

import eu.anastacia.monitoring.schemas.IoTSchemas.Attributes;
import eu.anastacia.monitoring.schemas.IoTSchemas.ContextElement;
import eu.anastacia.monitoring.schemas.IoTSchemas.ContextResponses;
import eu.anastacia.monitoring.schemas.IoTSchemas.Entity;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.kafka.*;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

public class IoTDataEnhancer {

    public static class DataEnhancer extends BaseBasicBolt {

        private static final AnastaciaLogger logger = new AnastaciaLogger();

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {

//            Sep 1 17:02:38 [AAA] {"source_ip":"aaaa::1", "source_port":"4000","affected_ip":"aaaa::2","affected_port":"716","type_of_device_affected":"PAA","event_type":"na"}


            List<Object> values = tuple.getValues();
            for(Object obj : values){
                Entity entity = null;
                String data = "";

                ObjectMapper mapper = new ObjectMapper();
                try{
                    entity = mapper.readValue(obj.toString(), Entity.class);
                    System.out.println("\n\n\n" + obj.toString());

                    List<ContextResponses> contextResponses = entity.getContextResponses();

                    for(ContextResponses response : contextResponses){
                        ContextElement contextElement = response.getContextElement();

                        List<Attributes> attributes = response.getContextElement().getAttributes();
                        String timestamp = "";
                        String source_ip = "null";
                        String source_port = "null";
                        String affected_ip = "null";
                        String affected_port = "null";
                        String type_of_device_affected = "null";
                        String event_type = "null";
                        String url = "null";

                        String identificator = "null";
                        String temperature = "null";

                        for(Attributes attribute : attributes){
                            switch (attribute.getName()){
                                case "timestamp":
                                    timestamp = attribute.getValue();
                                    break;
                                case "source_ip":
                                    source_ip = attribute.getValue();
                                    break;
                                case "sourceIP":
                                    source_ip = attribute.getValue();
                                    break;
                                case "source_port":
                                    source_port = attribute.getValue();
                                    break;
                                case "affected_ip":
                                    affected_ip = attribute.getValue();
                                    break;
                                case "affected_port":
                                    affected_port = attribute.getValue();
                                    break;
                                case "type_of_device_affected":
                                    type_of_device_affected = attribute.getValue();
                                    break;
                                case "event_type":
                                    event_type = attribute.getValue();
                                    break;
                                case "url":
                                    url = attribute.getValue();
                                    break;
                                case "temperature":
                                    temperature = attribute.getValue();
                                    break;
                                case "identificator":
                                    identificator = attribute.getValue();
                                    break;
                                default:
                                    System.out.println("We don't know about this attribute: " + attribute.getName());
                                    break;
                            }
                        }

                        Boolean sent = false;

                        if(contextElement.getType().equals("error")){
                            if(event_type.equals("na")){
                                data = " [AAA] {\"source_ip\":\"" + source_ip +
                                        "\",\"source_port\":\"" + source_port +
                                        "\",\"affected_ip\":\"" + affected_ip +
                                        "\",\"affected_port\":\"" + affected_port +
                                        "\",\"type_of_device_affected\":\"" + type_of_device_affected +
                                        "\",\"event_type\":\"" + event_type +
                                        "\"}";
                                sent = true;
                            }else if(event_type.equals("da")){
                                data = " [AAA] {\"source_ip\":\"" + source_ip +
                                        "\",\"source_port\":\"" + source_port +
                                        "\",\"affected_ip\":\"" + affected_ip +
                                        "\",\"affected_port\":\"" + affected_port +
                                        "\",\"type_of_device_affected\":\"" + type_of_device_affected +
                                        "\",\"event_type\":\"" + event_type +
                                        "\",\"url\":\"" + url +
                                        "\"}";
                                sent = true;
                            }else if(event_type.equals("dp")){
                                data = " [AAA] {\"source_ip\":\"" + source_ip +
                                        "\",\"source_port\":\"" + source_port +
                                        "\",\"affected_ip\":\"" + affected_ip +
                                        "\",\"affected_port\":\"" + affected_port +
                                        "\",\"type_of_device_affected\":\"" + type_of_device_affected +
                                        "\",\"event_type\":\"" + event_type +
                                        "\",\"url\":\"" + url +
                                        "\"}";
                                sent = true;
                            }
                        }else if(contextElement.getType().equals("IoTdevice")){
                            data = " [IOT] {\"identificator\":\"" + identificator +
                                    "\",\"temperature\":\"" + temperature +
                                    "\"}";
                        }


                        if(sent) {

                            final String OLD_FORMAT = "yyyy-MM-dd/HH:mm:ss";  //    2018-06-13/09:34:32
                            final String NEW_FORMAT = "MMM dd HH:mm:ss";

                            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
                            Date d = sdf.parse(timestamp);
                            sdf.applyPattern(NEW_FORMAT);
                            timestamp = sdf.format(d);
                            data = timestamp + data;

                            System.out.println(data + "\n\n\n");
                            //Produce data to the MonitoringDataEnhancerLogs topic
                            logger.send((long) 0, data);
                        }
                    }
                }
                catch (Exception e) {
                    System.out.println("Cast failed: " + e);
                }
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word", "count"));
        }

    }

    public static void main(String[] args) throws Exception {

        TopologyBuilder builder = new TopologyBuilder();


        String topicName = "IoTBrokerTopic";
        String zkRoot = "";
        String consumerGroupId = "group1";

        String kafka_url = System.getenv("KAFKA_URL_CONSUMER");

        BrokerHosts hosts = new ZkHosts(kafka_url);

        SpoutConfig spoutConfig = new SpoutConfig(hosts, topicName, zkRoot, consumerGroupId);
        spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        builder.setSpout("spout", kafkaSpout, 5);
        builder.setBolt("count", new IoTDataEnhancer.DataEnhancer(), 12).shuffleGrouping("spout");

        Config conf = new Config();
        conf.setDebug(false);

        if (args != null && args.length > 0) {
            conf.setNumWorkers(3);
            StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
        }
        else {
            conf.setMaxTaskParallelism(3);
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("word-count", conf, builder.createTopology());

            cluster.wait();

            Thread.sleep(10000);
            cluster.shutdown();
        }
    }




}
