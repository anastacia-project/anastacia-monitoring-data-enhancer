package eu.anastacia.monitoring;

import eu.anastacia.monitoring.schemas.mmtschema.MMTReport;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.kafka.*;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.cloudbees.syslog.Facility;
import com.cloudbees.syslog.Severity;

public class MMTProbe {

    public static class MMTReportFormatter extends BaseBasicBolt {

        private static final long serialVersionUID = 1915263558047088796L;

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            // Process the CSV data. This will produce an MMTReport object
            MMTReport report = MMTReport.processData(tuple.getString(0));

            // Transform the MMTReport object into its JSON representation
            String json = report.getJSON();

            // Emit the tuple to the next Bolt
            collector.emit(new Values(json));
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("json"));
        }
    }

    /* 
     * Name of the Kafka Channels to read from. The names of
     * these topics HAVE TO match the configuration of MMT-Probe.
     * The values provided here are the DEFAULT ones.
     */
    private static String TOPIC_STATS_REPORTS = "session.flow.report";
    private static String TOPIC_SEC_REPORTS = "security.report";
    private static String TOPIC_STATS_NO_SESS = "protocol.stat";

    // This string is the name of the Kafka Channels to write to
    private static String TOPIC_OUT_REFORMATTED = "security.syslog";

    public static void main(String[] args) throws Exception {
        String KAFKA_URL_CONSUMER = System.getenv("KAFKA_URL_CONSUMER");
        String outputKafkaServer = System.getenv("KAFKA_URL_PRODUCER");

        /* Configure the options */
        Options options = new Options();
        options.addOption(new Option("s", "sec-topic", true, "Kafka Topic containing the MMT Security Report (default: security.report)"));
        options.addOption(new Option("t", "stats-topic", true, "Kafka Topic containing the MMT Flow Statistics Report (default: session.flow.report)"));
        options.addOption(new Option("p", "proto-topic", true, "Kafka Topic containing the MMT No-Flow Statistics Report (default: protocol.stat"));
        options.addOption(new Option("o", "output", true, "Kafka Topic to write the syslog-formatted MMT-Reports (default: security.syslog)"));

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try{
            cmd = parser.parse(options, args);

            // Get the value of the options if they are specified
            if (cmd.getOptionValue("sec-topic") != null)
                TOPIC_SEC_REPORTS = cmd.getOptionValue("sec-topic");

            if (cmd.getOptionValue("stats-topic") != null)
                TOPIC_STATS_REPORTS = cmd.getOptionValue("stats-topic");

            if (cmd.getOptionValue("proto-topic") != null)
                TOPIC_STATS_NO_SESS = cmd.getOptionValue("stats-topic");

            if (cmd.getOptionValue("output") != null)
                outputKafkaServer = cmd.getOptionValue("stats-topic");
        } catch (ParseException e){
            System.out.println(e.getMessage());
            formatter.printHelp("MMT-Probe Storm Formatter", options);
            System.exit(1);
        }

        TopologyBuilder builder = new TopologyBuilder();
        BrokerHosts hosts = new ZkHosts(KAFKA_URL_CONSUMER);

        // Configuration for Security Reports
        SpoutConfig spoutSecConfig = new SpoutConfig(hosts, TOPIC_SEC_REPORTS, "/"+TOPIC_SEC_REPORTS, "group1");
        spoutSecConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        // Configuration for Session Stat Reports
        SpoutConfig spoutStatsConfig = new SpoutConfig(hosts, TOPIC_STATS_REPORTS, "/"+TOPIC_STATS_REPORTS, "group2");
        spoutStatsConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        // Configuration for No-Session Stat Reports
        SpoutConfig spoutNoSessStatsConfig = new SpoutConfig(hosts, TOPIC_STATS_NO_SESS, "/"+TOPIC_STATS_NO_SESS, "group3");
        spoutNoSessStatsConfig.scheme = new SchemeAsMultiScheme(new StringScheme());

        // Kafka Spout for Session Stat Reports
        KafkaSpout kafkaStatsSpout = new KafkaSpout(spoutStatsConfig);
        // Kafka Spout for No-Session Stat Reports
        KafkaSpout kafkaNoSessSpout = new KafkaSpout(spoutNoSessStatsConfig);
        // Kafka Spout for Security Report
        KafkaSpout kafkaSecSpout = new KafkaSpout(spoutSecConfig);

        // Build the Storm topology to execute
        // Configure both spouts
        builder.setSpout("stats", kafkaStatsSpout, 5);
        builder.setSpout("security", kafkaSecSpout, 5);
        builder.setSpout("no-sess", kafkaNoSessSpout, 5);
        // Connect all spouts to the MMTReportFormatter Bolt
        builder.setBolt("json-sec", new MMTProbe.MMTReportFormatter(), 12)
        .shuffleGrouping("security")
        .shuffleGrouping("stats")
        .shuffleGrouping("no-sess");

        // Prepare the SyslogFormatterBolt
        SyslogFormatterBolt syslogBolt = new SyslogFormatterBolt();
        syslogBolt.setAppName("MMT-Probe");
        syslogBolt.setFacility(Facility.ALERT);
        syslogBolt.setSeverity(Severity.ALERT);
        syslogBolt.setHostname("mmt-probe.host");

        builder.setBolt("syslog-json-sec", syslogBolt, 12).shuffleGrouping("json-sec");
        // We now set a new Bolt to send to the CNR Logging Service.
        // Note we connect this new bolt to the same "json-sec" bolt
        builder.setBolt("cnr-logging", new AnastaciaCNRLogging("MMT-Probe", "Monitoring", "Data Filtering", "Monitoring"))
        .shuffleGrouping("json-sec");

        Config conf = new Config();
        conf.setDebug(false);

        /*
        if (args != null && args.length > 0) {
            System.out.println("IF");
            conf.setNumWorkers(3);
            StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
        }
        else {
            System.out.println("ELSE");
            */
        conf.setMaxTaskParallelism(3);
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("mmt-processor", conf, builder.createTopology());

        cluster.wait();

        Thread.sleep(10000);
        cluster.shutdown();
        /*
        }
        */
    }
}
