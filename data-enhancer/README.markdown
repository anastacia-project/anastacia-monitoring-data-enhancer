# Example Storm Topologies

Learn to use Storm!

---

Table of Contents

* <a href="#getting-started">Getting started</a>


---


<a name="getting-started"></a>

# Getting started
1)	Storm Reads from a Kafka Topic 
2)	Enchances the received data and arranges 'em into a specific format
3)	Store them on a Mongo DB.

